package damo.cs.upc.edu.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import java.util.UUID;

/**
 * Created by josepm on 30/6/16.
 */
public class CrimListActivity extends SingleFragmentActivity {

    private Estat estat = new Estat();

    @Override
    protected void onResume() {
        super.onResume();
    }

    // ---------------------------------------------------------------

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        estat.desa(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        estat.recupera(savedInstanceState);

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.mestredetall;
    }

    @Override
    protected int getContenidorFragmentResId() {
        return R.id.contenidor;
    }

    @Override
    protected Fragment getInstance() {
        return new CrimListFragment();
    }

    @Override
    public void onCanviFragment(Crim crim, int idCrida) {
        switch (idCrida) {
            case FragmentObservable.MODIF_CRIM:
                if (estat.idDarrerVisualitzat == null) return;
                if (estat.idDarrerVisualitzat.compareTo(crim.getId()) == 0)
                    modificaDetall(crim);

                break;
            case FragmentObservable.NOVA_SELECCIÖ:
                estat.idDarrerVisualitzat = crim.getId();
                mostraDetall(crim);
                break;

            case FragmentObservable.EDICIO_CRIM:
                Log.v("LOG", "onCanvi CrimListActivity");
                modificaLlistat();
                break;

        }
    }

    private void modificaLlistat() {
        CrimListFragment fragment = (CrimListFragment) getFragmentManager().findFragmentById(R.id.contenidor);
        fragment.modificaLlistat();
    }

    private void mostraDetall(Crim crim) {

        //if (mobilApaisat()) {
        if (existeixFragmentDetall()) {
            FragmentManager fragmentManager = getFragmentManager();

            Fragment fragment = CrimFragment.getInstance(crim.getId());

            fragmentManager.beginTransaction().replace(R.id.contenidor_detall, fragment).commit();
        } else
            startActivity(new Intent(CrimActivity.getIntent(this, crim.getId())));
    }

    private boolean mobilApaisat() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    private boolean existeixFragmentDetall() {
        return findViewById(R.id.contenidor_detall) != null;
    }

    // Codi per l'exercici que només es construeix el CrimFRagment un sol cop
    private void mostraDetallDos(Crim crim) {

        if (getCrimFragment() != null)
            getCrimFragment().crimModificat(crim);
        else
            nouDetall(crim);
    }

    private void nouDetall(Crim crim) {
        CrimFragment fragment = (CrimFragment) CrimFragment.getInstance(crim.getId());

        getFragmentManager().beginTransaction().replace(R.id.contenidor_detall, fragment).commit();

        //  fragment.setObservadorFragment(this);
    }

    private void modificaDetall(Crim crim) {
        getCrimFragment().crimModificat();
    }

    private CrimFragment getCrimFragment() {
        return (CrimFragment) getFragmentManager().findFragmentById(R.id.contenidor_detall);
    }

    // ---------------------------------------------------------------
    private class Estat {
        public static final String DARRER_ID_CRIM = "Darrer id crim";

        public UUID idDarrerVisualitzat = null;


        public void desa(Bundle bundle) {
            // Protegim pel cas que hi hagi una rotació sense haver seleccionat prèviament cap crim
            if (estat.idDarrerVisualitzat != null) {
                bundle.putString(Estat.DARRER_ID_CRIM, estat.idDarrerVisualitzat.toString());
            }
        }

        public void recupera(Bundle bundle) {
            String s = bundle.getString(DARRER_ID_CRIM);
            if (s != null) {
                idDarrerVisualitzat = UUID.fromString(s);
            }
        }
    }
}
